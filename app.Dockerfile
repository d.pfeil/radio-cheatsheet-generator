FROM nginx:1.21

COPY ./dist/radio-cheatsheet-generator /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf