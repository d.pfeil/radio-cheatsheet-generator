# Radio Cheatsheet Editor

This project will contain a german and an english version (second part).
Dieses Projekt enthält einen Deutschen und einen Englischen Teil (second part).

## Funk Taschenkarten Generator (Deutscher Abschnitt)

Dieses Projekt soll die Möglichkeit bieten, eine Funk Taschenkarte automatisch zu erzeugen.
Dazu kann man die aktuelle Development Version unter https://derpfeil.gitlab.io/radio-cheatsheet-generator/ eingesehen werden.
Aktuell gibt es noch einige Dinge die ich gerne einbauen möchte. Diese findest du in der Featurelist.
Falls du noch Änderungswünsche hast melde dich doch einfach entweder mit einem Issue, Mail truemmerdackel@danis-bu.de, in der Hermine (OV Calw) oder über Mastodon @truemmerdackel.
Möchtest du mich unterstützten würde ich mich über einen Kaffee freuen.

<a href="https://www.buymeacoffee.com/D.pfeil"><img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=D.pfeil&button_colour=FF5F5F&font_colour=ffffff&font_family=Cookie&outline_colour=000000&coffee_colour=FFDD00"></a>

Ansonsten kannst du auch gerne Code contributen. Schau dazu doch bitte in die Contribute.md
Würde mich freuen von euch zu hören.

### Featurelist

Es gibt verschiedene Features, die ich noch einbauen möchte.

* Hinzufügen neuer Seiten
* Header der Seiten stylen
* Vorderseite stylen
* Style fürs Drucken als PDF hinzufügen
* Funkrufnamenrichtlinie als Preset auswählbar
* Speicherung im Browser
* Download als Datei (Image, PDF)
* Export zur Weitergabe oder Weiterbearbeitung
* Installations- und Entwicklungsanleitung ins Readme oder extra Datei

## Radio Cheatsheet Editor (englisch section)

This project should help you to generate a cheatsheet for your radio codes.
Actually you can try the development version under: https://derpfeil.gitlab.io/radio-cheatsheet-generator/.
There is also a Featurelist with the next upcoming features.
If you have any Feature request, issue or want to give feedback feel free to do this with an issue, per mail at truemmerdackel@danis-bu.de and on Mastodon @truemmerdackel.
You can also support me by buying a coffee: 

<a href="https://www.buymeacoffee.com/D.pfeil"><img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=D.pfeil&button_colour=FF5F5F&font_colour=ffffff&font_family=Cookie&outline_colour=000000&coffee_colour=FFDD00"></a>

If you want to contribute Code please read the contribution file.
It would be a pleasure to here from you.
### Featurelist

* function for adding new sheets
* style header of the single sheets
* style front cheet
* styles for print to pdf
* add the guidelines for radio code of the german federal agency for technical relief (I`m member of this, if you can send me the guidelines from your organization than I can add it to the list)
* save in the browser
* download as file (Image, PDF)
* export for sharing oder later editing
* Installation guide

### Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.4.

#### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

#### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

#### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

#### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

#### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
