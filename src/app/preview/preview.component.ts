import { Component } from '@angular/core';
import { EditorService } from '../editor/services/editor.service';
import { Cheatsheet } from '../model/Cheatsheet';
import { CheatsheetService } from '../services/cheatsheet/cheatsheet.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent {
  public cheatsheetService: CheatsheetService;
  public editorService: EditorService;

  constructor(cheatsheetService: CheatsheetService, editorService: EditorService) {
    this.cheatsheetService = cheatsheetService;
    this.editorService = editorService;
  }

  selectCheatsheet(cheatsheet: Cheatsheet){
    this.editorService.selectCheatsheet(cheatsheet);
  }
}
