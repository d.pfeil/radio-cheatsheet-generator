import { TestBed } from '@angular/core/testing';

import { Cheatsheet } from '../../model/Cheatsheet';
import { CheatsheetService } from './cheatsheet.service';

describe('CheatsheetService', () => {
  let service: CheatsheetService = new CheatsheetService();
  let cheatsheets: Cheatsheet[];

  beforeEach(() => {  
    cheatsheets = [{
        id: 1,
        title: "Sheet1",
        entries: []
      },
      {
        id: 2,
        title: "Sheet2",
        entries: []
      },
    ];

    TestBed.configureTestingModule({
      providers: [{provide: 'cheatsheets', useValue: [{
          id: 1,
          title: "Sheet1",
          entries: []
        },
        {
          id: 2,
          title: "Sheet2",
          entries: []
        },
      ]}]
    });
    service = TestBed.inject(CheatsheetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return cheatsheets', (done: DoneFn) => {
    service.cheatsheets.subscribe((cheatsheetsResult: Cheatsheet[]) => {
      expect(cheatsheetsResult).toEqual(cheatsheets);
      done();
    });
  })

  it('should add cheatsheet', (done: DoneFn) => {
    const add = {
      id: 3,
      title: "Sheet3",
      entries: []
    };

    service.addOrReplaceCheatsheet(add);
    cheatsheets.push(add)

    service.cheatsheets.subscribe((cheatsheetsResult: Cheatsheet[]) => {
      expect(cheatsheetsResult).toEqual(cheatsheets);
      done();
    });
  });

  it('should replace cheatsheet', (done: DoneFn) => {
    const add = {
      id: 2,
      title: "SheetRenamed",
      entries: []
    };

    cheatsheets.splice(1);
    cheatsheets.push(add);

    service.addOrReplaceCheatsheet(add);

    service.cheatsheets.subscribe((cheatsheetsResult: Cheatsheet[]) => {
      expect(cheatsheetsResult).toEqual(cheatsheets);
      done();
    });
  });

  it('should remove cheatsheet', (done: DoneFn) => {
    service.removeCheatsheet(cheatsheets[0]);

    service.cheatsheets.subscribe((cheatsheetsResult: Cheatsheet[]) => {
      expect(cheatsheetsResult).toEqual([{id: 2, title: "Sheet2", entries: []}]);
      done();
    });
  });
});
