import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Cheatsheet } from '../model/Cheatsheet';
import { Entry } from '../model/Entry';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnChanges, OnInit {
  @Input()
  public cheatsheet: Cheatsheet | null = {
    id: 1,
    title: "Titel",
    entries: [
      {id: 1, title: "Zeile 1"},
      {id: 2, title: "Zeile 2"},
      {id: 2, title: "Zeile 3"},
    ]
  }

  public cheatsheetForm = new FormGroup({
    title: new FormControl(''),
    entries: new FormArray([])
  });

  get entries() {
    return this.cheatsheetForm.get('entries') as FormArray;
  }

  get title() {
    return this.cheatsheetForm.get('title');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes["cheatsheet"].currentValue){
      const entriesFormArray = this.cheatsheetForm.get("entries") as FormArray;
      entriesFormArray.clear();

      const cheatsheet = changes["cheatsheet"].currentValue as Cheatsheet;

      this.cheatsheetForm.get("title")?.setValue(cheatsheet.title);
  
      cheatsheet.entries.forEach(entry => {
        entriesFormArray.push(new FormControl(entry.title))
      })
    }
  }

  ngOnInit(){
    const entriesFormArray = this.cheatsheetForm.get("entries") as FormArray;
    entriesFormArray.clear();

    const cheatsheet = this.cheatsheet as Cheatsheet;

    this.cheatsheetForm.get("title")?.setValue(cheatsheet.title);

    cheatsheet.entries.forEach(entry => {
      entriesFormArray.push(new FormControl(entry.title))
    })
  }

  onSubmit(): void {
    const entries: Entry[] = [];
    this.entries.value.forEach((entry: string, index: number) => {
      entries.push({
        id: index,
        title: entry
      })
    });

    this.cheatsheet!.entries = entries;
    this.cheatsheet!.title = this.title?.value;
  }

  moveEntryUp(index: number): void {
    if(index > 0){
      const entriesFormArray = this.entries;
      const entrieValues = entriesFormArray.value as String[];
      const reorderedEntries = this.swap(entrieValues, index - 1, index)
      entriesFormArray.setValue(reorderedEntries);
    }
  }

  moveEntryDown(index: number): void {
    const entriesFormArray = this.entries;
    const entrieValues = entriesFormArray.value as String[];
    if(index >= 0 && index < (entrieValues.length - 1)){
      const reorderedEntries = this.swap(entrieValues, index, index + 1)
      entriesFormArray.setValue(reorderedEntries);
    }
  }

  removeEntry(index: number): void {
    this.entries.removeAt(index);
  }

  addEntry(): void{
    this.entries.push(new FormControl(''));
  }

  swap(arr: any[], index1: number, index2: number): any[] {
    arr = [...arr];
    const temp = arr[index1];
    arr[index1] = arr[index2];
    arr[index2] = temp;
    return arr;
  }
}
