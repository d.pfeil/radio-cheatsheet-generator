import { TestBed } from '@angular/core/testing';
import { Cheatsheet } from 'src/app/model/Cheatsheet';

import { EditorService } from './editor.service';

describe('EditorService', () => {
  let service: EditorService;
  const cheatsheet: Cheatsheet = {
    id: 1,
    title: "Sheet1",
    entries: []
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: 'activeCheatsheet', useValue: cheatsheet}]
    });
    service = TestBed.inject(EditorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should match the inital cheatsheet correct', (done: DoneFn) => {
    service.cheatsheet.subscribe((cheatsheetResult: Cheatsheet) => {
      expect(cheatsheetResult).toEqual(cheatsheet);
      done();
    });
  })

  it('should update the cheatsheet correct', (done: DoneFn) => {
    const modifiedCheatsheet = {
      id: 2,
      title: "Sheet2",
      entries: [
        {
          id: 1,
          title: "test"
        }
      ]
    };
    service.selectCheatsheet(modifiedCheatsheet);

    service.cheatsheet.subscribe((cheatsheetResult: Cheatsheet) => {
      expect(cheatsheetResult).toEqual(modifiedCheatsheet);
      done();
    });
  })
});
