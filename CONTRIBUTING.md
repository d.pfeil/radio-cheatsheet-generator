# Contributing

I/We would love to receive Merge Requests from everyone containing your improvements or every other form of contribution.

## Preparation
To do so please fork the project and clone it:

* SSH
`git clone git@gitlab.com:<namespace>/radio-cheatsheet-generator.git`

* HTTPS
`git clone https://gitlab.com/d.pfeil/radio-cheatsheet-generator.git`

## Running & Make your changes

See the description in the Running Section of the Readme

## Quality

Please write tests for your section and check that no other test or the linter is failing.

## Publish

Push to the fork and open a Merge Request with the original Project.
Then I/we will check your changes and merge.
