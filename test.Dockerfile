FROM node:current-alpine3.12
WORKDIR /app
ENV CHROME_BIN=/usr/bin/chromium-browser

RUN apk add chromium

CMD npm run test